lib.name = motex
class.sources = \
	getenv.c \
	ln~.c \
	pansig~.c \
	pan~.c \
	pol2rec~.c \
	polygate~.c \
	rec2pol~.c \
	shuffle.c \
	$(empty)

define forLinux
class.sources += \
	system.c \
	$(empty)
endef

datafiles = \
	$(wildcard *.pd) \
	$(wildcard *.txt) \
	$(wildcard *.md) \
	$(empty)

datadirs = \
	examples \
	$(empty)

PDLIBBUILDER_DIR=/usr/share/pd-lib-builder
include $(PDLIBBUILDER_DIR)/Makefile.pdlibbuilder
